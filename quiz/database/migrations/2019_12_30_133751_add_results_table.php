<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddResultsTable extends Migration
{
    /**
     * Run the migrations.
     * Creates the results table with its relations
     *
     * @return void
     */
    public function up()
    {
        Schema::create('results', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id'); // fk user
            $table->unsignedBigInteger('quiz_id'); // fk played quiz
            $table->dateTime('playedAt');
            $table->double('score', 8, 2); // 8-digit enum with a precision of 2 decimal digits
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('quiz_id')->references('id')->on('quizzes')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     * Drops the results table if exists
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('results');
    }
}
