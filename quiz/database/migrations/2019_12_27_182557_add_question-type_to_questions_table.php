<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddQuestionTypeToQuestionsTable extends Migration
{
    /**
     * Run the migrations.
     * Adds the column "type", enum with all possible questions types
     * 
     * @return void
     */
    public function up()
    {
        Schema::table('questions', function (Blueprint $table) {
            $table->enum('type', ['SingleChoice', 'MultipleChoice']);
        });
    }

    /**
     * Reverse the migrations.
     * Drops th column "type" if exists
     *
     * @return void
     */
    public function down()
    {
        if(Schema::hasColumn('questions', 'type')) {
            Schema::table('questions', function (Blueprint $table) {
                $table->dropColumn('type');
            });
        }
    }
}
