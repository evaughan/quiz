<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePlayedAtIndexResultsTable extends Migration
{
    /**
     * Run the migrations.
     * Creates an index on the date column "playedAt" on the results table
     *
     * @return void
     */
    public function up()
    {
        Schema::table('results', function (Blueprint $table) {
            $table->index('playedAt');
        });
    }

    /**
     * Reverse the migrations.
     * Deletes the index for "playedAt" on the results table
     *
     * @return void
     */
    public function down()
    {
        Schema::table('results', function (Blueprint $table) {
            $table->dropIndex('playedAt');
        });
    }
}
