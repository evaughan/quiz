<?php

namespace App\Http\Controllers;

use App\Result;
use App\User;
use App\Quiz;
use Illuminate\Http\Request;

class ResultController extends Controller
{
    /**
     * Register middleware and auth for resources in ctor
     */
    public function __construct()
    {        
        $this->middleware('auth:api');
        $this->authorizeResource(Result::class, 'result');
    }

    /**
     * Returns all result entities
     * 
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $results = Result::all();

        return response($results);
    }

    /**
     * Returns all results from a specific user
     * 
     * @param User user to load the results for
     * 
     * @return \Illuminate\Http\Response
     */
    public function indexForUser(User $user)
    {
        $resultsFromUser = Result::ofUser($user);

        return response($resultsFromUser);
    }

    /**
     * Returns all results from a specific quiz
     * 
     * @param Quiz quiz to load the results for
     * 
     * @return \Illuminate\Http\Response
     */
    public function indexForQuiz(Quiz $quiz)
    {
        $resultsFromQuiz = Result::ofQuiz($quiz);

        return response($resultsFromQuiz);
    }

    /**
     * Returns all results from a specific quiz but only the top score for each user
     * 
     * @param Quiz quiz to load the results for
     * 
     * @return \Illuminate\Http\Response
     */
    public function indexForQuizTop(Quiz $quiz)
    {
        $topResultsFromQuiz = Result::topResultsByQuiz($quiz);

        return response($topResultsFromQuiz);
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Result result to display
     * 
     * @return \Illuminate\Http\Response
     */
    public function show(Result $result)
    {
        return response($result);
    }

    /**
     * Stores a new result in the db
     * 
     * @param \Illuminate\Http\Request $request
     * 
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'quiz_id' => 'required|numeric|min:0|not_in:0', // value > 0
            'score' => 'required|numeric|min:0' // value >= 0
        ]);

        $result = new Result([
            'quiz_id' => $request->get('quiz_id'),
            'score' => $request->get('score')
        ]);

        $result->save();

        return response($result, 201);
    }
}