<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Auth;

class Result extends Model
{
    protected $fillable = [
        'quiz_id',
        'score'
    ];

    /**
     * Override for boot function to set the current users id for the results relation to the user
     */
    public static function boot() 
    {
        parent::boot();
        static::creating(function($result) {
            $user = Auth::user();
            $result->user_id = $user->id;
            $result->playedAt = date("Y-m-d H:i:s"); // current datetime
        });
    }

    /**
     * Returns all results from a specific user
     * 
     * @param Builder query builder
     * @param User user object which must contain the id of the user
     * 
     * @return array
     */
    public function scopeOfUser(Builder $query, User $user)
    {
        return $query->where('user_id', $user->id)->get();
    }

    /**
     * Returns all results from a specific quiz including the users name
     * 
     * @param Builder query builder
     * @param Quiz quiz object which must contain the id of the quiz
     * 
     * @return array
     */
    public function scopeOfQuiz(Builder $query, Quiz $quiz)
    {
        return $query->where('quiz_id', $quiz->id)
        ->join('users', 'results.user_id', '=', 'users.id')
        ->addSelect('users.name')
        ->get();
    }

    /**
     * Returns for each user his top result measured by the score for the requested quiz including the users name
     * 
     * @param Quiz quiz which results are requested
     * 
     * @return array
     */
    public static function topResultsByQuiz(Quiz $quiz)
    {
        return \DB::table('results')
        ->selectRaw('results.id, results.quiz_id, results.user_id, users.name,  max(results.score) as score, results.playedAt')
        ->join('users', 'results.user_id', '=', 'users.id')
        ->where('results.quiz_id', $quiz->id)
        ->groupBy('users.id')
        ->get();
    }

    /**
     * Returns the player / user which achieved this result
     * 
     * @return object
     */
    public function player()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Returns the quiz which this result was achieved for
     * 
     * @return object
     */
    public function quiz()
    {
        return $this->belongsTo(Quiz::class);
    }
}