<?php

namespace App\Utilities;

class DbEnumHelper {
    /**
     * Retrieves the acceptable enum fields for a column
     * https://stackoverflow.com/questions/26991502/get-enum-options-in-laravels-eloquent
     *
     * @param string $table Table name
     * @param string $column Column name
     *
     * @return array
     */
    public static function getPossibleEnumValues ($table, $column)
    {
        // Pulls column string from DB
        $enumStr = \DB::select(\DB::raw('SHOW COLUMNS FROM '. $table .' WHERE Field = "'. $column .'"'))[0]->Type;

        // Parse string
        preg_match_all("/'([^']+)'/", $enumStr, $matches);

        // Return matches
        return isset($matches[1]) ? $matches[1] : [];
    }
}
