import Axios from "axios";
import { store } from '../store';

const baseURL = `/api`;

export const axios = Axios.create({
    baseURL,
    headers: {'Authorization': `Bearer ${store.state.authentication.user.token}`}
});