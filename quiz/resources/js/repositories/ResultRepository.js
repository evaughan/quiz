import Repository from 'axios';

const resource = '/results';

export default {
    getAll() {
        return Repository.get(`${resource}`);
    },

    getForUser(userId) {
        return Repository.get(`users/${userId}${resource}`);
    },

    getForQuiz(quizId) {
        return Repository.get(`quizzes/${quizId}${resource}`);
    },

    getTopForQuiz(quizId) {
        return Repository.get(`quizzes/${quizId}${resource}/top`);
    },

    getResult(resultId) {
        return Repository.get(`${resource}/${resultId}`);
    },

    createResult(payload) {
        return Repository.post(`${resource}`, payload);
    }

}