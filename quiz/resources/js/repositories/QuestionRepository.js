import Repository from 'axios';

const resource = '/questions';

export default {
    get() {
        return Repository.get(`${resource}`);
    },

    getForQuiz(quizId) {
        return Repository.get(`quizzes/${quizId}${resource}`);
    },

    getQuestion(questionId) {
        return Repository.get(`${resource}/${questionId}`);
    },

    getAllTypes() {
        return [
            {
                text: 'Single Choice',
                value: 'SingleChoice'
            },
            {
                text: 'Multiple Choice',
                value: 'MultipleChoice'
            }
        ]
    },

    createQuestion(payload) {
        return Repository.post(`${resource}`, payload);
    },
}