import EventBus from './EventBus';

export const eventKeys = {
    message: "MESSAGE_PUSHED"
};

export const EventBroker = {
    push(key, payload) {
        EventBus.$emit(key, payload);
    },

    subscribe(key, callback) {
        EventBus.$on(key, callback);
    },

    subscribeForOnce(key, callback) {
        EventBus.$once(key, callback);
    },

    unsubscribe(key, callback) {
        EventBus.$off(key, callback);
    }
};