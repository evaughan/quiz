import Vue from 'vue';
import Vuex from 'vuex';
import Vuetify from 'vuetify';
import App from './components/App';
import Axios from 'axios';
import colors from 'vuetify/lib/util/colors';
import '@mdi/font/css/materialdesignicons.css';
import { router } from './routing';
import { store } from './store';

Vue.use(Vuetify);

export default new Vuetify({
    theme: {
        dark: false,
        themes: {
            dark: {
                primary: colors.purple,
                secondary: colors.grey.darken1,
                accent: colors.shades.black,
                error: colors.red.accent3,
            },
        },
    },
});

Axios.defaults.headers.common['Content-Type'] = 'application/json';
Axios.defaults.baseURL = '/api';

// if user was previously logged in set token as default auth request header
if (store.state.authentication.status.loggedIn) {
    Axios.defaults.headers.common['Authorization'] = `Bearer ${store.state.authentication.user.token}`;
}

const app = new Vue({
    el: '#app',
    components: {App},
    router: router,
    store: store,
    vuetify: new Vuetify({
        icons: 'mdi',
        theme: {
            dark: false,
            themes: {
                dark: {
                    primary: colors.green,
                    secondary: colors.grey.darken1,
                    accent: colors.shades.black,
                    error: colors.red.accent3,
                },
            },
        },
    })
});