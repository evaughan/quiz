import Vue from 'vue';
import VueRouter from 'vue-router';
import routes from './routes';

Vue.use(VueRouter);

export const router = new VueRouter({
    mode: 'history',
    routes: routes
});

const publicRoutes = routes.filter(r => r.anonymous).map(r => r.path.toLowerCase());

router.beforeEach((to, from, next) => {
    // redirect to login page if not logged in and trying to access a restricted page
    const authRequired = !publicRoutes.includes(to.path.toLowerCase());
    const loggedIn = localStorage.getItem('user');

    if (authRequired && !loggedIn) {
        return next('/login');
    }

    next();
});