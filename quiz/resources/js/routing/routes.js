import Login from "./../components/Auth/Login";
import NewQuiz from "./../components/quiz/NewQuiz";
import QuizList from "./../components/quiz/QuizList";
import QuizView from "./../components/quiz/QuizView";
import NewQuestion from "./../components/question/NewQuestion";
import SignUp from "./../components/Auth/SignUp";
import QuizPlay from "./../components/quiz/QuizPlay";
import Logout from "./../components/Auth/Logout";
import Home from "./../components/Home";
import UnknownRoute from "./../components/navigation/UnknownRoute.vue";

const routes = [
    {
        path: '/',
        name: 'default',
        component: Home,
        anonymous: true
    },
    {
        path: '/signup',
        name: 'sign up',
        component: SignUp,
        anonymous: true
    },
    {
        path: '/login',
        name: 'login',
        component: Login,
        anonymous: true
    },
    {
        path: '/logout',
        name: 'logout',
        component: Logout,
        anonymous: false
    },
    {
        path: '/new-quiz',
        name: 'new quiz',
        component: NewQuiz,
        anonymous: false
    },
    {
        path: '/quizzes',
        name: 'list of quizzes',
        component: QuizList,
        anonymous: false
    },
    {
        path: '/quizzes/:quizId',
        name: 'show single quiz',
        component: QuizView,
        anonymous: false
    },
    {
        path: '/quizzes/:quizId/new-question',
        name: 'add question to quiz',
        component: NewQuestion,
        anonymous: false,
        props: (route) => {
            const props = {...route.params};
            props.quizId = +props.quizId;
            return props;
        }
    },
    {
        path: '/quizzes/:quizId/play',
        name: 'play quiz',
        component: QuizPlay,
        anonymous: false,
        props: (route) => {
            const props = {...route.params};
            props.quizId = +props.quizId;
            return props;
        }
    },
    {
        path: '*',
        name: 'unknown route',
        component: UnknownRoute,
        anonymous: false
    }
];

export default routes;