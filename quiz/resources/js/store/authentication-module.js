import { MessageService, UserService,  userDataStorageKey} from '../services';
import { router } from '../routing';

const user = JSON.parse(localStorage.getItem(userDataStorageKey));
const initialState = user ? { status: { loggedIn: true }, user, signUp: { errors: null }} : { status: {}, user: null, signUp: { errors: null } };

export const authentication = {
    namespaced: true,
    state: initialState,
    actions: {
        login({ commit }, { email, password }) {
            commit('loginRequest', { email });

            UserService.login(email, password)
                .then(user => {
                    commit('loginSuccess', user);
                    router.push('/quizzes');
                })
                .catch(error => {
                    commit('loginFailure', error);
                    MessageService.warning('Invalid credentials, please try again');
                });
        },
        logout({ commit }) {
            UserService.logout();
            commit('logout');
        },
        signUp({ commit }, { name, email, password, passwordConfirmation }) {
            UserService.signUp(name, email, password, passwordConfirmation)
                .then(user => {
                    commit('signUpSuccess', user);
                    router.push('/quizzes');
                })
                .catch(error => {
                    commit('signUpFailure', error.response.data.error);
                });
        }
    },
    mutations: {
        loginRequest(state, user) {
            state.status = { loggingIn: true };
            state.user = user;
        },
        loginSuccess(state, user) {
            state.status = { loggedIn: true };
            state.user = user;
        },
        loginFailure(state) {
            state.status = {};
            state.user = null;
        },
        logout(state) {
            state.status = {};
            state.user = null;
        },
        signUpSuccess(state, user) {
            state.user = user;
            state.signUp.errors = null;
        },
        signUpFailure(state, errors) {
            state.signUp.errors = errors;
        }
    }
}