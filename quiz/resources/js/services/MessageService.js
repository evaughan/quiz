import {EventBroker, eventKeys} from './../events';

export const messageTypes = {
    info: "infoMessage",
    success: "successMessage",
    warning: "warningMessage",
    error: "errorMessage"
}

export const MessageService = {
    info(message) {
        this.send(messageTypes.info, message);
    },

    success(message) {
        this.send(messageTypes.success, message);
    },

    warning(message) {
        this.send(messageTypes.warning, message);
    },

    error(message) {
        this.send(messageTypes.error, message);
    },

    send(type, message) {
        if(!Object.values(messageTypes).includes(type)) {
            throw new Error("Provided message type is not supported");
        }

        EventBroker.push(eventKeys.message, {
            type: type,
            message: message
        });
    },

    listen(callback) {
        EventBroker.subscribe(eventKeys.message, callback);
    }
}