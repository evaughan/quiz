const months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];

export const DateService = {
    getFormatedDate() {
        this.getFormatedDate(new Date());       
    },

    getFormatedDate(dateObj) {
        if (!dateObj instanceof Date) {
            throw new TypeError("Provided date object isn't an instance of Date");
        }
        const day = dateObj.getDate();
        const month = months[dateObj.getMonth()];
        const year = dateObj.getFullYear();

        const minute = dateObj.getMinutes();
        const time = `${dateObj.getHours()}:${(minute<10?'0':'') + minute}`;

        return `${day}. ${month} ${year} ${time}`;
    }
}