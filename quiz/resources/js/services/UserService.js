import Axios from'axios';

export const userDataStorageKey = 'user';

export const UserService = {
    login,
    logout,
    signUp,
    getProfile
}

function login(email, password) {
    return Axios.post('/login', {
        email: email,
        password: password
    })
        .then(response => response.data.success)
        .then(tokenData => {
            // set token as default auth request header
            Axios.defaults.headers.common['Authorization'] = `Bearer ${tokenData.token}`;

            return getProfile()
                .then(userData =>  {
                    userData.token = tokenData.token;
                    storeUser(userData);

                    return userData;
                })
        });
}

function logout() {
    // remove user and his token from local storage to log out
    localStorage.removeItem(userDataStorageKey);
}

function signUp(name, email, password, passwordConfirmation) {
    return Axios.post('/signUp', {
        name: name,
        email: email,
        password: password,
        password_confirmation: passwordConfirmation
    })
        .then(response => response.data.success)
        .then(tokenData => {
            // set token from new user as default auth request header
            Axios.defaults.headers.common['Authorization'] = `Bearer ${tokenData.token}`;

            return getProfile()
                .then(userData => {
                    userData.token = tokenData.token;
                    storeUser(userData);

                    return userData;
                });
        });
}

function getProfile() {
    return Axios.get('profile')
        .then(response => response.data.success);
}

function storeUser(user) {
    localStorage.setItem(userDataStorageKey, JSON.stringify(user));
}